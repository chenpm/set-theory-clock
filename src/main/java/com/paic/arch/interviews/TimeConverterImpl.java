package com.paic.arch.interviews;

import org.apache.commons.lang.StringUtils;

public class TimeConverterImpl implements TimeConverter {
	private static final String DEFAULT_VALUE = "0"; // 默认值
	private static final String LIGHT_COLOR_RED = "R"; // 红灯
	private static final String LIGHT_COLOR_YELLOW = "Y"; // 黄灯
    
	@Override
	public String convertTime(String aTime) {
		if(aTime == null || aTime.isEmpty())
			throw new IllegalArgumentException("parameter is not allowed null or empty");
		if(aTime.indexOf(":") < 0)
			throw new IllegalArgumentException("invalid time format, example: HH:mm:ss");
		
		String[] timeSegment = aTime.split(":");
		if(timeSegment.length != 3)
			throw new IllegalArgumentException("invalid time format, example: HH:mm:ss");
		
		int hour = Integer.parseInt(timeSegment[0]);
		int minute = Integer.parseInt(timeSegment[1]);
		int second = Integer.parseInt(timeSegment[2]);
				
		StringBuilder clockBuilder = new StringBuilder();
		
        clockBuilder.append(getSecondSection(second)).append("\r\n");
        
        clockBuilder.append(getFirstHourSection(hour)).append("\r\n");
        clockBuilder.append(getSecondHourSection(hour)).append("\r\n");
        
        clockBuilder.append(getFirstMinuteSection(minute)).append("\r\n");
        clockBuilder.append(getSecondMinuteSection(minute));
        
        return clockBuilder.toString();
	}
	
	/**
	 * 获取秒
	 * @param second
	 * @return
	 */
	private String getSecondSection(int second) {
        String result = DEFAULT_VALUE;
        
        if(second % 2 == 0) {
        	result = LIGHT_COLOR_YELLOW;
        }

        return result;
	}
	
	/**
	 * 获取第一部分的小时
	 * @param hour
	 * @return
	 */
	private String getFirstHourSection(int hour) {
        String result = "";
        
        int number = hour / 5;
        for(int i = 0; i <  number; i++) {
        	result = LIGHT_COLOR_RED + result;
        }

        result = StringUtils.rightPad(result, 4, DEFAULT_VALUE);
        
        return result;
	}
	/**
	 * 获取第二部分的小时
	 * @param hour
	 * @return
	 */
	private String getSecondHourSection(int hour) {
        String result = "";
        
        int number = hour % 5;
        for(int i = 0; i < number; i++) {
            result = LIGHT_COLOR_RED + result;
        }
        result = StringUtils.rightPad(result, 4, DEFAULT_VALUE);
        
        return result;
	}
	
	/**
	 * 获取第一部分的分钟
	 * @param minute
	 * @return
	 */
	private String getFirstMinuteSection(int minute) {
        String result = "";
        
        int number = minute / 5;
        for(int i = 0; i < number; i++) {
            String lightColor;
            switch(i){
                case 2:
                case 5:
                case 8:
                    lightColor = LIGHT_COLOR_RED;
                    break;
                default:
                    lightColor = LIGHT_COLOR_YELLOW;
            }
            result += lightColor;
        }
        result = StringUtils.rightPad(result, 11, DEFAULT_VALUE);

        return result;
	}
	/**
	 * 获取第二部分的分钟
	 * @param minute
	 * @return
	 */
	private String getSecondMinuteSection(int minute) {
        String result = "";
        
        int number = minute % 5;
        for(int i = 0; i < number; i++) {
            result = LIGHT_COLOR_YELLOW + result;
        }
        result = StringUtils.rightPad(result, 4, DEFAULT_VALUE);

        return result;
	}
}
